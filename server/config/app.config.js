// require('dotenv').config();

const config = {};

config.dbHost = process.env.dbHost || "next-scene-dev.x87js.mongodb.net";
config.dbPort = process.env.dbPort || "27017";
config.dbName = process.env.dbName || "next-scene";
config.dbusername = process.env.dbusername || "dev";
config.dbpassword = process.env.dbpassword || "dev123";

export default config;
