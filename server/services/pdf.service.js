import pdf from "pdf-creator-node";
import fs from "fs";

export const createFile = async (data) => {
  console.log(data);
  let html = fs.readFileSync("template.html", "utf8");
  let options = {
    format: "A4",
    orientation: "portrait",
    border: "10mm",
    header: {
      height: "45mm",
      contents: '<div style="text-align: center;">Omnilytics</div>',
    },
    footer: {
      height: "28mm",
      contents: {
        first: "Cover page",
        2: "Second page", // Any page number is working. 1-based index
        default:
          '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
        last: "Last Page",
      },
    },
  };

  let document = {
    html: html,
    data: {
      items: data,
    },
    path: "./output.pdf",
    type: "",
  };

  let result = new Promise(async (resolve, reject) => {
    pdf
      .create(document, options)
      .then((res) => {
        console.log(res);
        resolve(res);
      })
      .catch((error) => {
        console.error(error);
        reject(error);
      });
  });
  return await result;
};
