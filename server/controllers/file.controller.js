import { uploadFile, deleteFile } from "../services/aws.service";
import pdf from "pdf-creator-node";
import { createFile } from "../services/pdf.service";

const FileController = {};

FileController.create = async (req, res) => {
  //this is the method

  try {
    const fileObj = req.body;
    await createFile(fileObj);
    let data = await uploadFile();
    res.status(200).json({
      error: false,
      message: "File Created",
      data: data,
    });
  } catch (error) {
    res.status(500).json({
      error: true,
      message: "error Creating File",
    });
  }
};

export default FileController;
