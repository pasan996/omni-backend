import express from "express";
import FileController from "../controllers/file.controller";
import CostController from "../controllers/file.controller";
var router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});

router.post("/create", function (req, res, next) {
  FileController.create(req, res);
});

export default router;
