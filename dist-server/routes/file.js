"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _file = _interopRequireDefault(require("../controllers/file.controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = _express.default.Router();
/* GET users listing. */


router.get("/", function (req, res, next) {
  res.send("respond with a resource");
});
router.post("/create", function (req, res, next) {
  _file.default.create(req, res);
});
var _default = router;
exports.default = _default;