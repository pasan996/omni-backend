"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _aws = require("../services/aws.service");

var _pdfCreatorNode = _interopRequireDefault(require("pdf-creator-node"));

var _pdf = require("../services/pdf.service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const FileController = {};

FileController.create = async (req, res) => {
  //this is the method
  try {
    const fileObj = req.body;
    await (0, _pdf.createFile)(fileObj);
    let data = await (0, _aws.uploadFile)();
    res.status(200).json({
      error: false,
      message: "File Created",
      data: data
    });
  } catch (error) {
    res.status(500).json({
      error: true,
      message: "error Creating File"
    });
  }
};

var _default = FileController;
exports.default = _default;