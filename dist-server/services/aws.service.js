"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.deleteFile = exports.uploadFile = void 0;

var _awsSdk = _interopRequireDefault(require("aws-sdk"));

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireWildcard(require("path"));

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

//configuring the AWS environment
const uploadFile = async () => {
  _awsSdk.default.config.update({
    accessKeyId: "AKIAJYJH7KMOUEO45DYA",
    secretAccessKey: "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"
  });

  var s3 = new _awsSdk.default.S3();

  let file = _fs.default.readFileSync("output.pdf"); //configuring parameters


  var params = {
    Bucket: "next-scene-data",
    Body: file,
    Key: "data.pdf"
  };
  let result = new Promise(async (resolve, reject) => {
    s3.upload(params, (err, data) => {
      //handle error
      let imageData;

      if (err) {
        console.log("Error", err);
        reject(err);
      } //success


      if (data) {
        console.log("Uploaded in:", data.Location);
        imageData = {
          key: data.key,
          location: data.Location
        };
        resolve(imageData);
      }
    });
  });
  return await result;
};

exports.uploadFile = uploadFile;

const deleteFile = async key => {
  _awsSdk.default.config.update({
    accessKeyId: "AKIAJYJH7KMOUEO45DYA",
    secretAccessKey: "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"
  });

  console.log("this is from the aws", key);
  var s3 = new _awsSdk.default.S3(); //configuring parameters

  var params = {
    Bucket: "next-scene-data",
    Key: key
  };
  let result = new Promise(async (resolve, reject) => {
    s3.deleteObject(params, (err, data) => {
      //handle error
      let imageData;

      if (err) {
        console.log("Error", err);
        reject(err);
      } //success


      if (data) {
        console.log("Deleted in:", data);
        imageData = data;
        resolve(imageData);
      }
    });
  });
  return await result;
};

exports.deleteFile = deleteFile;